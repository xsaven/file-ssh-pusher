'use strict';

const chokidar = require('vscode-chokidar');
const fs = require('fs');
const program = require('commander');
const path = require('path');
const Client = require('ssh2').Client;
const _ = require('lodash');

let watcher;

const test_path = (sftp, path, cb) => {
    sftp.exists(path, function(exist) {
        if (exist) {
            cb('ok');
        }else {
            console.log('Creating "'+path+'"...');
            sftp.mkdir(path, 777, function (err) {
                if (err) {
                    console.log("Error in directory creation: %s", path);
                }else {
                    console.log('Created "'+path);
                    cb('new');
                }
            });
        }
    });
};

const see_change_files = (project_dir, file, cfg) => {
    let locale_file = path.join(project_dir, file);
    let server_file = path.join(cfg.ssh.dir, file).replace(/\\/g, '/');

    let conn = new Client();
    conn.on('ready', function() {
        conn.sftp(function(err, sftp) {
            let paths = path.parse(file).dir.split('\\');
            let state = [];
            let iteration = 0;
            let upload = () => {
                let options = Object.assign({}, {encoding: 'utf-8'}, true);
                let writeStream = sftp.createWriteStream(server_file, options);
                let data = writeStream.end(fs.readFileSync(locale_file));
                writeStream.on('close', function() {
                    let d = new Date();
                    console.log(`${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}`+'> sync: '+file);
                    conn.end();
                });
            };
            let go = () => {
                test_path(sftp, path.join(cfg.ssh.dir, state.join('/'), paths[iteration]).replace(/\\/g, '/'), ($status)=>{
                    if(paths[(iteration+1)]!=undefined){
                        state.push(paths[iteration]);
                        iteration++;
                        go();
                    }else upload();
                });
            };
            go();
        });
    }).connect(cfg.ssh);
};

const start_watcher = (dir, cfg) => {
    const conn = new Client();
    console.log('Start connecting to to SHH...');
    conn.on('ready', function() {
        conn.sftp(function(err, sftp) {
            if (err) {
                console.log("Error in connection SSH", err);
                process.exit(0);
            } else {
                console.log("Connection established!");
                console.log("Scanner established!");
                let ignore = new RegExp("/(^|[\\/\\\\])\\..|"+cfg.ignored.join('|')+"/");
                watcher = chokidar.watch(dir, {ignored: ignore}).on('all', (event, path, details) => {
                    path = path.replace(dir,'');
                    path = path=='' ? '\\' : path;
                    //console.log(event, path);
                    if(event=='change') {
                        let p = cfg.paths[event];
                        if(_.find(p, function(o) { return path.indexOf(o)>=0; })){
                            see_change_files(dir, path, cfg);
                        }
                    }
                });
                conn.end();
            }
        });
    }).connect(cfg.ssh);
};

program
    .version('0.0.1')
    .command('dir <req>')
    .description('command description')
    .option('-o, --option','we can still have add options')
    .action(function(req,optional){
        if(fs.existsSync(req)){
            if(!fs.existsSync(path.join(req, 'watcher_config.json'))){
                let def = {
                    ssh: {
                        host: "",
                        port: 22,
                        username: "",
                        password: "",
                        dir: "./"
                    },
                    ignored: [
                        "node_modules",
                        "vendor",
                        "storage",
                        ".idea",
                        ".git",
                        ".gitignore",
                        ".gitattributes"
                    ],
                    paths: {
                        change: []
                    }
                };
                fs.writeFile(path.join(req, 'watcher_config.json'), JSON.stringify(def), () => {
                    console.log('A configuration file was created, edit: %s; And run it again.', path.join(req, 'watcher_config.json'));
                    fs.writeFile(path.join(req, 'run_watcher.bat'), '@START "File watcher - '+req+'" node '+path.join(__dirname, 'bin', 'www')+' dir '+req, () => {
                        console.log('Now you have starter fo to this project: '+path.join(req, 'run_watcher.bat'));
                        process.exit(0);
                    });
                });
            }else{
                console.log('Starting...');
                start_watcher(req, JSON.parse(fs.readFileSync(path.join(req, 'watcher_config.json'))));
            }
        }else{
            console.log('Don\'t start watcher, not found path: %s', req);
            process.exit(0);
        }
    });
program.parse(process.argv);

module.exports = watcher;